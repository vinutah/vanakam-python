__author__ = 'Vinu Joseph'

for i in range(1, 20):
    print("i is now {}".format(i))

##################################################

number = "9,223,372,036,854,775,807"
for i in range(0, len(number)):
    print(number[i])

##################################################

number = "9,223,372,036,854,775,807"
for i in range(0, len(number)):
    if (number[i] in '0123456789'):
        print(number[i])
        # automatically adds new line

##################################################

number = "9,223,372,036,854,775,807"
for i in range(0, len(number)):
    if (number[i] in '0123456789'):
        print(number[i], end='')

##################################################
print("\n")

number = "9,223,372,036,854,775,807"
cleanedNumber = ''

for i in range(0, len(number)):
    if number[i] in '0123456789':
        cleanedNumber = cleanedNumber + number[i]

newNumber = int(cleanedNumber)
print("The newNumber is {}".format(newNumber))

##################################################

number = "9,223,372,036,854,775,807"
cleanedNumber = ''

for char in number:
    if char in '0123456789':
        cleanedNumber = cleanedNumber + char

newNumber = int(cleanedNumber)
print("The number is {}".format(newNumber))

##################################################

"""
EXTENDING FOR LOOPS
"""

for state in ["not pinin", "no more", "a stiff", "bereft of life"]:
    print("This parrot is {}".format(state))

print("==================")
for i in range(0, 100, 5):
    print("i is {} ".format(i))

print("==================")
for i in range(1,13):
    for j in range(1,13):
        print("{} times {} is {}".format(i, j, i*j))
    print("==================")


for i in range(1,13):
    for j in range(1,13):
        print("{} times {} is {}".format(i, j, i*j), end='\t')
    print("")











