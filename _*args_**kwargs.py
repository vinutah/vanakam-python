def say_hello_then_call_f(f, *args, **kwargs):
    print("args = {}".format(args))
    print("kwargs = {}".format(kwargs))
    print("calling {}".format(f))
    print(f(*args, **kwargs))


def g(x, y, z=1):
    return (x + y)/z

say_hello_then_call_f(g, 1, 2, z = 5)
