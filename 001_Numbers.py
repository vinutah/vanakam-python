age = 28
print(age)

# print(greeting + age)


# Traceback (most recent call last):
# 28
# File "/Users/vinu/Desktop/PythonPrograms/later-3.py", line 46, in <module>
# print(greeting + age)
# TypeError: must be str, not int

print(greeting + str(age))

# =========================
# Built in Data Types
# =========================
#
# Numerical
# =========================
# Int (Now Long)
# Long (Removed)

#
# no fractional part
# whole numbers
#

#
# no fraction after decimal
# computations are faster that float
# keep integers unless needed.
#

# integers has a max limit

a = 12
print(a)

b = 3
print(b)

# Python no declarations before use
# Python do not abuse flexibility
#
# INTEGER
# LONG INTEGER
# FLOAT
# DOUBLE

print(a + b)

print(a * b)

# Returns Float

print(a / b)
print(type(a / b))

# Returns whole number

print(a // b)
print(type(a // b))

print(a % b)


# Float
# largest positive number
# smallest negative number

# Complex

# // is important
#
# Traceback (most recent call last):
# File "/Users/vinu/Desktop/PythonPrograms/later-3.py", line 50, in <module>
# for i in range(1,a / b):
# TypeError: 'float' object cannot be interpreted as an integer

# for i in range(1,a / b):
#     print(i)

#
# arithmetic on numbers
# operators supported on numbers
#

# operator precedence
# not always left to right
# / * + -

print(a + b / 3 - 4 * 12)

# use paren freely

print((((a + b) / 3) - 4) * 12)

# breaking computations

c = a + b
d = c / 3
e = d - 4
print(e * 12)