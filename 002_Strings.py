__author__ = 'vinu'

greeting = "Hello"
name = input("Please enter your name ")
print(greeting + ' ' + name)

splitString = "This string has been \nsplit over\nseveral\nlines"
print(splitString)

tabbedString = "1\t2\t3\t4\t5\t"
print(tabbedString)
print('the pet shop owner said "No, no \'e\'s uh,...he\'s resting"')
print("the pet shop owner said \"No, no 'e's uh,...he's resting\"")


anotherSplitString = """This string has been 
split over
several lines
"""
print(anotherSplitString)

print('''The pet shop owner said "No, no, 'e's uh,...he's resting"''')
print("""The pet shop owner said "No, no, 'e's uh,...he's resting" """)

# Sequences
# .String

parrot = "Norwegian Blue"
print(parrot)


print(len(parrot))

# Indexing Strings

print(parrot[0])
print(parrot[1])
print(parrot[2])
print(parrot[3])
print(parrot[4])
print(parrot[5])
print(parrot[6])
print(parrot[7])
print(parrot[8])
print(parrot[9])
print(parrot[10])
print(parrot[11])
print(parrot[12])
print(parrot[13])

# print(parrot[14])
# IndexError: string index out of range

print(parrot[-1])

#

print(parrot[0:6])
print(parrot[:6])

#

print(parrot[6:])

# Negative Indexing

print(parrot[-4:-2])

#

print(parrot[0:6:2])
print(parrot[0:6:3])

#

numbers = "9,223,372,036,854,775,807"
print(numbers[1::4])

#

numbers = "1, 2, 3, 4, 5, 6 ,7, 8, 9"
print(numbers[0::3])

# Methods on Strings

# Add strings

string1 = "he's "
string2 = "probably "
print(string1 + string2)

# Multiply strings

print("Vanakam " * 5)
print("Vanakam " * (5 * 4))

# Sub strings methods

today = "friday"
print("day" in today)
print("fri" in today)
print("sat" in today)
print("sun" in "sat")


# Formatting String and Numbers

age = 24
print("My age is " + str(age) + " years")
print("My age is {0} years".format(age))

# syntax introduced in python3

print("""
January  : {2}
February : {0}
March    : {2}
April    : {1}
May      : {2}
June     : {1}
July     : {2}
August   : {2}
September: {1} 
October  : {2}
November : {1}
December : {2}
""".format(28, 30, 31))

#
# deprecated feature
#

print("Pi is approximately %12.50f" % (22 / 7))

#
# formatting operator % ??
#

print("My age is %d years" % age)
print("My age is %d %s, %d %s" % (age, "years", 6, "months"))

for i in range(1, 12):
    print("No. %2d squared is %4d and cubed is %4d" %(i, i ** 2, i ** 3))

for i in range(1, 12):
    print("No. {} squared is {} and cubed is {}".format(i, i ** 2, i ** 3))

# new replacement field syntax, specifying the field, using the colon operator

# field width
# left alignment

for i in range(1, 12):
    print("No. {0:2} squared is {1:4} and cubed is {2:4}".format(i, i ** 2, i ** 3))

for i in range(1, 12):
    print("No. {0:2} squared is {1:<4} and cubed is {2:<4}".format(i, i ** 2, i ** 3))

print("Pi is approximately {0:12.50f}".format(22 / 7))



