__author__ = 'vinu'

# The Plan ?

# Numbers
# Sequences
# . Strings
# . Lists
# Mapping
# Files
# Classes
# Instances
# Exceptions

# print function

print("Hello World")
print(1+2)
print(7+6)
print()
print("The End")


# quotes
# single - double quotes

print("Python string are easy to use")
print('We can even include "quotes" in strings')
print("hello" + " world")

# store in variables

greeting = "Hello"
name = "Ganesh"

# methods on strings

print(greeting + name)

# if we want a space, we can add that too

print(greeting + ' ' + name)

# Naming rules

# start with a letter
greeting = "Vanakam "

# can start with a underscore

_myName = "Vinu"

# 1Vinu
# File "/Users/vinu/Desktop/PythonPrograms/later-3.py", line 5
# 1Vinu
# ^
# SyntaxError: invalid syntax

# Name can start with upper case
# and can have numbers

Vinu22 = "Good"
Vinu_Was_22 = "Good"

#
# Python variables are case sensitive
#
Greeting = "There"

#
# Memory allocated when allocating a value
#

print(Vinu_Was_22 + ' ' + greeting)