some_list = ['do', 'the', 'right', 'thing', 'right', 'things', 'will', 'happen']
print("A LIST {}".format(some_list))

mapping = dict((k, v) for k, v in enumerate(some_list))
print("A DICT {}".format(mapping))

mapping = dict((k, v) for v, k in enumerate(some_list))
print("A DICT {}".format(mapping))


