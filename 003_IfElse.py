__author__ = 'Vinu Joseph'

# program flow

# for loop
# indentation and braces
# automatic indentation
# do not need delimiters
# at-least 1 space
# block of code
# body of a function is a block

for i in range(1, 12):
    print("No {} squared is {:4} and cubed is {:5}".format(i, i ** 2, i ** 3))
print("calculation complete")
print("try again")

# if program flow

name = input('Please enter your name: ')
age = int(input('How old are you, {0}?'.format(name)))
print(age)

# processing based on the age

if age >= 18:
    print("Yor are old enough to Code")
    print("Start reading the Gordon Book for PL Theory, Today")
else:
    print("Please come back in {0} years".format(18 - age))

print("Please guess a number between 1 and 10: ")
guess = int(input())

if guess < 5:
    print('Please guess higher')
    guess = int(input())

    # duplicated code

    if guess == 5:
        print('Well done, you guesses it')
    else:
        print('Sorry you have not guessed correctly')

elif guess > 5:
    print('Please guess lower')
    guess = int(input())

    # duplicated code

    if guess == 5:
        print('Well done, you guessed it')
    else:
        print("Sorry, you have not guessed correctly")

else:
    print("You got it the first time")

# Second iteration to develop the code

print("Please guess a number between 1 and 10: ")
guess = int(input())

if guess != 5:
    if guess < 5:
        print('Please guess higher')
    else:# guess is greater than 5
        print('Please guess lower')

    guess = int(input())

    if guess == 5:
        print("Well done, you guessed it")
    else:
        print("Sorry, you have not guessed correctly")

else:
    print("You got it first time")

age = int(input("how old are you? "))

if (age >= 16) and (age <= 65):
    print("happy coding! in python")

if 15 < age < 65:
    print("happy coding! in scala")

# both expressions are evaluated

if (age < 16) or (age > 65):
    print("happy reading! novels")
else:
    print("happy coding in c++")

# if (condition) or (function_call):
    # do something

# No boolean data type
# has true and false
# There is a bool function

# non empty value = true

x = 'false'

if x:
    print("x is true")

# false = 0
# any non 0 = true

# replacement field

print("""
False              : {0} 
None               : {1}
0                  : {2}
0.0                : {3}
empty list []      : {4}
empty tuple ()     : {5}
empty string ''    : {6}
empty string ""    : {7}
empty mapping {{}} : {8}
""".format(False,
           bool(None), bool(0),
           bool(0.0),  bool([]),
           bool(()), bool(''),
           bool(""), bool({})))

x = int(input("Please enter some text"))

if x:
    print("You entered '{}'".format(x))
else:
    print("You did not enter anything")


# operations on boolean

print(not False)
print(not True)

age = int(input("How old are you"))
if not(age < 18):
    print("You are old enough to code")
    print("Register for cs2100 and cs3100")
else:
    print("Please come back in {0} years".format(18 - age))

parrot = "Norwegien Blue"

letter = input("Enter a character: ")

if letter in parrot:
    print("Give me an {}, Bob".format(letter))
else:
    print("I don't need that letter")


# Write a small program to ask for a name and an age
# When both values have been entered, check if the person
# is the right age to go on an 18-30 holiday (they must be
# over 18 and under 31).
# If they are, welcome t hem to the holiday
# Otherwise print a polite message refusing them entry


name = input("PLease enter you name")
age = int(input("Please enter you age :"))


if not (18 <= age <= 31):
    if (age < 18):
        print("{} come back after {} years".format(name,18 - age))
    else:
        print("{} are late by {} years".format(name,age - 31))
else:
    print("{} Welcome!".format(name))